package com.jobsity.challenge

import android.app.Application
import android.content.Context

class ChallengeApp: Application() {

    init {
        instance = this
    }

    companion object {
        private var instance: ChallengeApp? = null

        fun applicationContext() : Context {
            return instance!!.applicationContext
        }
    }
}