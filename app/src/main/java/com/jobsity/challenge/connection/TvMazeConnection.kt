package com.jobsity.challenge.connection

import android.util.Log
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.json.JSONObject
import java.io.*
import java.lang.Exception
import java.net.HttpURLConnection
import java.net.URL

class TvMazeConnection {

    companion object {
        private val TAG = TvMazeConnection::class.java.simpleName

        suspend fun getRequest(urlResource: String): String {

            val urlRemote: URL = URL(urlResource)

            val jsonStrRespuestaMAD = withContext(Dispatchers.IO) {

                try {
                    val inputStream: InputStream

                    // create HttpURLConnection
                    val conn: HttpURLConnection = urlRemote.openConnection() as HttpURLConnection
                    conn.requestMethod = "GET"
                    conn.connect()

                    // receive response as inputStream
                    inputStream = conn.inputStream

                    // convert inputstream to string
                    if (inputStream != null) {
                        convertInputStreamToString(inputStream)
                    }
                    else
                        generateJsonError()
                } catch (exception: IOException) {
                    Log.e(TAG, "*** ERROR IO:" + urlResource + " *** ", exception)
                    generateJsonError()
                } catch (exception: Exception) {
                    Log.e(TAG, " *** ERROR: " + urlResource + " *** ", exception)
                    generateJsonError()
                }
            }

            Log.i(TAG, "***** RES: " + urlResource + " *** " + jsonStrRespuestaMAD)
            return jsonStrRespuestaMAD
        }

        private fun convertInputStreamToString(inputStream: InputStream): String {
            val bufferedReader: BufferedReader? = BufferedReader(InputStreamReader(inputStream))

            var line:String? = bufferedReader?.readLine()
            var result = ""

            while (line != null) {
                result += line
                line = bufferedReader?.readLine()
            }

            inputStream.close()
            return result
        }

        /**
         * Method to generate a generic error response
         */
        private fun generateJsonError() : String {

            val jsonStrError = JSONObject().apply {
                put("code", "-001")
                put("message", "error")
            }.toString()

            return jsonStrError
        }
    }


}