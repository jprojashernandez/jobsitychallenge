package com.jobsity.challenge.connection

import com.jobsity.challenge.ChallengeApp
import com.jobsity.challenge.R
import com.jobsity.challenge.connection.params.InBase
import com.jobsity.challenge.connection.protocol.TvMazeTags
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

class TvMazeClient {

    companion object {

        suspend fun getTvShows(page: Int = 0): InBase {
            val result = TvMazeConnection.getRequest(getResourceUrl(R.string.CONFIG_TVMAZE_SER_SHOWS) + "?page=" + page)

            return try {
                var response = JSONArray(result)
                InBase(true, "", JSONObject().put(TvMazeTags.TAG_SHOWS, response))
            } catch (e: JSONException) {
                var error = JSONObject(result)
                InBase(false, "", error)
            }
        }

        suspend fun getSeasons(idTvShow: Int): InBase {
            val serviceUrl = getResourceUrl(R.string.CONFIG_TVMAZE_SER_SEASONS)
                .replace(":id", idTvShow.toString())

            val result = TvMazeConnection.getRequest(serviceUrl)

            return try {
                var response = JSONArray(result)
                InBase(true, "", JSONObject().put(TvMazeTags.TAG_SHOWS, response))
            } catch (e: JSONException) {
                var error = JSONObject(result)
                InBase(false, "", error)
            }
        }

        suspend fun getEpisodes(idSeason: Int): InBase {
            val serviceUrl = getResourceUrl(R.string.CONFIG_TVMAZE_SER_EPISODES)
                .replace(":id", idSeason.toString())

            val result = TvMazeConnection.getRequest(serviceUrl)
            return try {
                var response = JSONArray(result)
                InBase(true, "", JSONObject().put(TvMazeTags.TAG_EPISODES, response))
            } catch (e: JSONException) {
                var error = JSONObject(result)
                InBase(false, "", error)
            }
        }

        suspend fun searchShows(text: String): InBase {
            val serviceUrl = getResourceUrl(R.string.CONFIG_TVMAZE_SER_SEARCH_SHOWS)
                .replace(":text", text)

            val result = TvMazeConnection.getRequest(serviceUrl)
            return try {
                var response = JSONArray(result)
                InBase(true, "", JSONObject().put(TvMazeTags.TAG_SHOWS, response))
            } catch (e: JSONException) {
                var error = JSONObject(result)
                InBase(false, "", error)
            }
        }

        suspend fun searchPeople(text: String): InBase {
            val serviceUrl = getResourceUrl(R.string.CONFIG_TVMAZE_SER_SEARCH_PEOPLE)
                .replace(":text", text)

            val result = TvMazeConnection.getRequest(serviceUrl)
            return try {
                var response = JSONArray(result)
                InBase(true, "", JSONObject().put(TvMazeTags.TAG_PEOPLE, response))
            } catch (e: JSONException) {
                var error = JSONObject(result)
                InBase(false, "", error)
            }
        }

        suspend fun getCastCredits(idPerson: Int): InBase {
            val serviceUrl = getResourceUrl(R.string.CONFIG_TVMAZE_SER_CAST_CREDITS)
                .replace(":id", idPerson.toString())

            val result = TvMazeConnection.getRequest(serviceUrl)
            return try {
                var response = JSONArray(result)
                InBase(true, "", JSONObject().put(TvMazeTags.TAG_CAST_CREDITS, response))
            } catch (e: JSONException) {
                var error = JSONObject(result)
                InBase(false, "", error)
            }
        }

        private fun getResourceUrl(idUrlService: Int): String {
            val context = ChallengeApp.applicationContext()
            return String.format("%s%s",
                context.getString(R.string.CONFIG_CLIENT_TVMAZE_URL),
                context.getString(idUrlService))
        }
    }
}