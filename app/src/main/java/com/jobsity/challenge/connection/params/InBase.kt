package com.jobsity.challenge.connection.params

import org.json.JSONObject

open class InBase(val success: Boolean, val message: String, val response: JSONObject?) { }