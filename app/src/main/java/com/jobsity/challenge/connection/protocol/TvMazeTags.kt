package com.jobsity.challenge.connection.protocol

object TvMazeTags {

    const val TAG_CAST_CREDITS = "castCredits"
    const val TAG_COUNTRY = "country"
    const val TAG_DAYS = "days"
    const val TAG_EMBEDDED = "_embedded"
    const val TAG_EPISODE_ORDER = "episodeOrder"
    const val TAG_EPISODES = "episodes"
    const val TAG_GENRES = "genres"
    const val TAG_ID = "id"
    const val TAG_IMAGE = "image"
    const val TAG_MEDIUM = "medium"
    const val TAG_MESSAGE = "message"
    const val TAG_NAME = "name"
    const val TAG_NUMBER = "number"
    const val TAG_PEOPLE = "people"
    const val TAG_PERSON = "person"
    const val TAG_SEASON = "season"
    const val TAG_SCHEDULE = "schedule"
    const val TAG_SHOW = "show"
    const val TAG_SHOWS = "shows"
    const val TAG_SUMMARY = "summary"
    const val TAG_TIME = "time"
}