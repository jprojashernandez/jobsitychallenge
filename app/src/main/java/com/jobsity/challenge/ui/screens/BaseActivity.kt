package com.jobsity.challenge.ui.screens

import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.appbar.MaterialToolbar
import com.google.android.material.snackbar.Snackbar
import com.jobsity.challenge.R

open class BaseActivity : AppCompatActivity() {

    var layoutMain: View? = null

    var toolbarMain: MaterialToolbar? = null

    var layoutContainer: View? = null
    var layoutLoading: View? = null
    var layoutError: View? = null

    open fun bindToView() {
        layoutMain = findViewById(R.id.layout_main)
        toolbarMain = findViewById(R.id.toolbar_main)
        layoutContainer = findViewById(R.id.layout_container)
        layoutLoading = findViewById(R.id.layout_loading)
        layoutError = findViewById(R.id.layout_error)

        setupToolbar()
    }

    fun setupToolbar() {
        toolbarMain?.setNavigationOnClickListener {
            finish()
        }
    }

    fun showContent() {
        layoutContainer?.visibility = View.VISIBLE
        layoutLoading?.visibility = View.GONE
        layoutError?.visibility = View.GONE
    }

    fun showLoading() {
        layoutContainer?.visibility = View.GONE
        layoutLoading?.visibility = View.VISIBLE
        layoutError?.visibility = View.GONE
    }

    open fun showError() {
        layoutContainer?.visibility = View.GONE
        layoutLoading?.visibility = View.GONE
        layoutError?.visibility = View.VISIBLE
    }

    fun showSnackError() {
        layoutMain?.let {
            val snackbar = Snackbar.make(
                layoutMain!!,
                getString(R.string.generic_error_message),
                Snackbar.LENGTH_LONG
            )
            snackbar.show()
        }
    }
}