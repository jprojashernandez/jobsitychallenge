package com.jobsity.challenge.ui.model.adapters

import android.content.Context
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.jobsity.challenge.R
import com.jobsity.challenge.connection.protocol.TvMazeTags
import com.jobsity.challenge.utils.ChallengeUtils
import org.json.JSONArray
import org.json.JSONObject

class SeasonsAdapter (private val context: Context,
                      val jsonSeasons: JSONArray, val onItemClickListener: OnItemClickListener) : RecyclerView.Adapter<SeasonsAdapter.HolderSeason>() {

    private val inflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HolderSeason {
        val inflater = LayoutInflater.from(parent.context)
        return HolderSeason(context, inflater, parent)
    }

    override fun onBindViewHolder(holder: HolderSeason, position: Int) {
        val jsonCategoria = jsonSeasons.getJSONObject(position)
        holder.bind(position, jsonCategoria, onItemClickListener)
    }

    override fun getItemCount(): Int = jsonSeasons.length()

    interface OnItemClickListener {
        fun onItemClick(jsonSeason: JSONObject)
    }

    /**
     *
     */
    class HolderSeason(private val contexto: Context, inflater: LayoutInflater, parent: ViewGroup) :
        RecyclerView.ViewHolder(inflater.inflate(R.layout.item_season, parent, false)) {

        private var imgvImage: ImageView? = null
        private var tvName: TextView? = null
        private var tvEpisodes: TextView? = null

        init {
            imgvImage = itemView.findViewById(R.id.imgv_image) as ImageView
            tvName = itemView.findViewById(R.id.tv_name) as TextView
            tvEpisodes = itemView.findViewById(R.id.tv_episodes) as TextView
        }

        fun bind(position: Int, jsonSeason: JSONObject, onItemClickListener: OnItemClickListener) {
            itemView.setOnClickListener { onItemClickListener.onItemClick(jsonSeason) }

            val imageUrl = ChallengeUtils.getDefaultTvShowPoster(jsonSeason)
            ChallengeUtils.downloadImage(imageUrl, imgvImage!!, R.drawable.ic_image)

            var seasonName = jsonSeason.getString(TvMazeTags.TAG_NAME)
            tvName?.text = if (!TextUtils.isEmpty(seasonName)) {
                seasonName
            }
            else{
                String.format("Season %s", jsonSeason.getString(TvMazeTags.TAG_NUMBER))
            }

            val episodes = jsonSeason.optInt(TvMazeTags.TAG_EPISODE_ORDER, 0)
            if (episodes > 0) {
                tvEpisodes?.text = String.format("%s episodes", episodes)
            }
        }
    }
}