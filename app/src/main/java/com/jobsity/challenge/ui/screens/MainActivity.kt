package com.jobsity.challenge.ui.screens

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.AdapterView
import android.widget.ListView
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.widget.Toolbar
import androidx.drawerlayout.widget.DrawerLayout
import com.jobsity.challenge.R
import com.jobsity.challenge.ui.screens.home.HomeFragment
import com.jobsity.challenge.ui.model.adapters.SideMenuAdapter
import com.jobsity.challenge.ui.model.objects.MenuItem
import com.jobsity.challenge.ui.screens.people.PeopleFragment
import com.jobsity.challenge.ui.screens.search.SearchFragment

class MainActivity : AppCompatActivity() {

    lateinit var drawerLayout: DrawerLayout

    lateinit var lvSideMenu: ListView

    lateinit var sideMenuAdapter: SideMenuAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        drawerLayout = findViewById(R.id.drawer_layout)
        val toggle = ActionBarDrawerToggle(
            this,
            drawerLayout,
            toolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()

        // Get ui variables reference
        configureFragment()

        val listMenuItems: MutableList<MenuItem> = prepareSideMenuOptions()
        sideMenuAdapter = SideMenuAdapter(this, listMenuItems)
        lvSideMenu.adapter = sideMenuAdapter
        lvSideMenu.onItemClickListener =
            AdapterView.OnItemClickListener { _, _, position, id -> showSideMenuOption(position) }

        showSideMenuOption(0);
    }

    private fun configureFragment() {
        lvSideMenu = findViewById(R.id.lv_side_menu)
    }

    private fun prepareSideMenuOptions() : MutableList<MenuItem> {
        val listMenuItems: MutableList<MenuItem> = mutableListOf()

        val menuItemHome = MenuItem(
            R.drawable.ic_home,
            getString(R.string.side_menu_home), getString(R.string.side_menu_home), HomeFragment())
        menuItemHome.selected = true
        listMenuItems.add(menuItemHome)

        listMenuItems.add(
            MenuItem(
                R.drawable.ic_search,
                getString(R.string.side_menu_search),
                getString(
                    R.string.side_menu_search
                ),
                SearchFragment()
            )
        )

        listMenuItems.add(
            MenuItem(
                R.drawable.ic_people,
                getString(R.string.side_menu_people),
                getString(R.string.side_menu_people),
                PeopleFragment()
            )
        )

        return listMenuItems
    }

    private fun showSideMenuOption(position: Int) {
        sideMenuAdapter.selectMenuItem(position)
        val itemMenu = sideMenuAdapter.getItem(position)

        supportFragmentManager
            .beginTransaction()
            .replace(R.id.nav_host_fragment, itemMenu.fragment, "")
            .commit()

        supportActionBar!!.title = itemMenu.title
        drawerLayout.closeDrawers()
    }

    companion object {
        fun newInstance(context: Context): Intent {
            return Intent(context, MainActivity::class.java)
        }
    }
}