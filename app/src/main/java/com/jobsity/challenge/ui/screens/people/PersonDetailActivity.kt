package com.jobsity.challenge.ui.screens.people

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.jobsity.challenge.R
import com.jobsity.challenge.connection.TvMazeClient
import com.jobsity.challenge.connection.protocol.TvMazeTags
import com.jobsity.challenge.ui.model.adapters.CrewCreditsAdapter
import com.jobsity.challenge.ui.screens.BaseActivity
import com.jobsity.challenge.ui.screens.show.TvShowDetailActivity
import com.jobsity.challenge.utils.ChallengeUtils
import kotlinx.coroutines.launch
import org.json.JSONArray
import org.json.JSONObject

class PersonDetailActivity : BaseActivity() {

    lateinit var jsonPerson: JSONObject
    lateinit var jsonShows: JSONArray

    lateinit var pbShows: ProgressBar
    lateinit var layoutShows: LinearLayout
    lateinit var rvShows: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_person_detail)

        var jsonStrPerson = intent.getStringExtra("jsonPerson")
        jsonPerson = JSONObject(jsonStrPerson)

        bindToView()
        requestCastCredits()
    }

    override fun bindToView() {
        super.bindToView()

        val imgvPoster = findViewById<ImageView>(R.id.imgv_poster)
        val imageUrl = ChallengeUtils.getDefaultTvShowPoster(jsonPerson)
        ChallengeUtils.downloadImage(imageUrl, imgvPoster!!, R.drawable.ic_image)

        val tvName = findViewById<TextView>(R.id.tv_name)
        tvName.text = jsonPerson.getString(TvMazeTags.TAG_NAME)

        val tvCountry = findViewById<TextView>(R.id.tv_country)
        tvCountry.text = ChallengeUtils.getCountryText(jsonPerson)

        pbShows = findViewById(R.id.pb_shows)
        layoutShows = findViewById(R.id.layout_shows)

        rvShows = findViewById(R.id.rv_shows)
        rvShows.addItemDecoration(DividerItemDecoration(rvShows.context, LinearLayoutManager.VERTICAL))
    }

    private fun updateContent(jsonShows: JSONArray) {
        this.jsonShows = jsonShows
        val crewCreditsAdapter = CrewCreditsAdapter(this, this.jsonShows, object: CrewCreditsAdapter.OnItemClickListener {
            override fun onItemClick(jsonTvShow: JSONObject) {
                navigateToTvShowDetailActivity(jsonTvShow)
            }
        })
        rvShows.adapter = crewCreditsAdapter

        pbShows.visibility = View.GONE
        layoutShows.visibility = View.VISIBLE
    }

    override fun showError() {
        pbShows.visibility = View.GONE
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Navigation
    ////////////////////////////////////////////////////////////////////////////////////////////////

    fun navigateToTvShowDetailActivity(jsonTvShow: JSONObject) {
        val intent = TvShowDetailActivity.newInstance(this, jsonTvShow.toString())
        startActivity(intent)
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Service
    ////////////////////////////////////////////////////////////////////////////////////////////////

    private fun requestCastCredits() {
        pbShows.visibility = View.VISIBLE
        layoutShows.visibility = View.GONE

        lifecycleScope.launch {
            val response = TvMazeClient.getCastCredits(jsonPerson.getInt(TvMazeTags.TAG_ID))

            if (response.success) {
                updateContent(response.response!!.getJSONArray(TvMazeTags.TAG_CAST_CREDITS))
            }
            else {
                showError()
            }
        }
    }

    companion object {
        fun newInstance(context: Context, jsonPerson: String): Intent {
            val intent = Intent(context, PersonDetailActivity::class.java)
            intent.putExtra("jsonPerson", jsonPerson)
            return intent
        }
    }
}