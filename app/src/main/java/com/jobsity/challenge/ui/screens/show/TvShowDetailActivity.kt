package com.jobsity.challenge.ui.screens.show

import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.text.Html
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.jobsity.challenge.R
import com.jobsity.challenge.connection.TvMazeClient
import com.jobsity.challenge.connection.protocol.TvMazeTags
import com.jobsity.challenge.ui.model.adapters.SeasonsAdapter
import com.jobsity.challenge.ui.screens.BaseActivity
import com.jobsity.challenge.utils.ChallengeUtils
import kotlinx.coroutines.launch
import org.json.JSONArray
import org.json.JSONObject

class TvShowDetailActivity : BaseActivity() {

    lateinit var jsonTvShow: JSONObject
    lateinit var jsonSeasons: JSONArray

    lateinit var pbSeasons: ProgressBar
    lateinit var layoutSeasons: LinearLayout
    lateinit var rvSeasons: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tv_show_detail)

        var jsonStrTvShow = intent.getStringExtra("jsonTvShow")
        jsonTvShow = JSONObject(jsonStrTvShow)

        bindToView()
        requestSeasons()
    }

    override fun bindToView() {
        super.bindToView()

        val imgvPoster = findViewById<ImageView>(R.id.imgv_poster)
        val imageUrl = ChallengeUtils.getDefaultTvShowPoster(jsonTvShow)
        ChallengeUtils.downloadImage(imageUrl, imgvPoster!!, R.drawable.ic_image)

        val tvName = findViewById<TextView>(R.id.tv_name)
        tvName.text = jsonTvShow.getString(TvMazeTags.TAG_NAME)

        val tvGenres = findViewById<TextView>(R.id.tv_genres)
        tvGenres.text = ChallengeUtils.getGenresText(jsonTvShow)

        val tvSchedule = findViewById<TextView>(R.id.tv_schedule)
        tvSchedule.text = ChallengeUtils.getScheduleText(jsonTvShow)

        val tvSummary = findViewById<TextView>(R.id.tv_summary)
        val summary = jsonTvShow.getString(TvMazeTags.TAG_SUMMARY)
        tvSummary.text = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Html.fromHtml(summary, Html.FROM_HTML_MODE_COMPACT)
        } else {
            Html.fromHtml(summary)
        }

        pbSeasons = findViewById(R.id.pb_seasons)
        layoutSeasons = findViewById(R.id.layout_seasons)

        rvSeasons = findViewById(R.id.rv_seasons)
        rvSeasons.addItemDecoration(DividerItemDecoration(rvSeasons.context, LinearLayoutManager.VERTICAL))
    }

    private fun updateContent(jsonSeason: JSONArray) {
        val seasonsAdapter = SeasonsAdapter(this, jsonSeasons, object: SeasonsAdapter.OnItemClickListener {
            override fun onItemClick(jsonSeason: JSONObject) {
                navigateToEpisodesListActivity(jsonSeason)
            }
        })
        rvSeasons.adapter = seasonsAdapter

        pbSeasons.visibility = View.GONE
        layoutSeasons.visibility = View.VISIBLE
    }

    override fun showError() {
        pbSeasons.visibility = View.GONE
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Navigation
    ////////////////////////////////////////////////////////////////////////////////////////////////

    fun navigateToEpisodesListActivity(jsonSeason: JSONObject) {
        val intent = EpisodesListActivity.newInstance(this, jsonSeason.toString())
        startActivity(intent)
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Service
    ////////////////////////////////////////////////////////////////////////////////////////////////

    private fun requestSeasons() {
        pbSeasons.visibility = View.VISIBLE
        layoutSeasons.visibility = View.GONE

        lifecycleScope.launch {
            val response = TvMazeClient.getSeasons(jsonTvShow.getInt(TvMazeTags.TAG_ID))

            if (response.success) {
                jsonSeasons = response.response!!.getJSONArray(TvMazeTags.TAG_SHOWS)
                updateContent(jsonSeasons!!)
            }
            else {
                showError()
            }
        }
    }

    companion object {
        fun newInstance(context: Context, jsonTvShow: String): Intent {
            val intent = Intent(context, TvShowDetailActivity::class.java)
            intent.putExtra("jsonTvShow", jsonTvShow)
            return intent
        }
    }
}