package com.jobsity.challenge.ui.screens

import android.view.View
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import com.jobsity.challenge.R

open class BaseFragment: Fragment() {
    var layoutMain: View? = null
    var layoutContainer: View? = null
    var layoutLoading: View? = null
    var layoutError: View? = null

    open fun bindToView(viewMain: View) {
        layoutMain = viewMain.findViewById(R.id.layout_main)
        layoutContainer = viewMain.findViewById(R.id.layout_container)
        layoutLoading = viewMain.findViewById(R.id.layout_loading)
        layoutError = viewMain.findViewById(R.id.layout_error)
    }

    fun showContent() {
        layoutContainer?.visibility = View.VISIBLE
        layoutLoading?.visibility = View.GONE
        layoutError?.visibility = View.GONE
    }

    fun showLoading() {
        layoutContainer?.visibility = View.GONE
        layoutLoading?.visibility = View.VISIBLE
        layoutError?.visibility = View.GONE
    }

    fun showError() {
        layoutContainer?.visibility = View.GONE
        layoutLoading?.visibility = View.GONE
        layoutError?.visibility = View.VISIBLE
    }

    fun showSnackError() {
        layoutMain?.let {
            val snackbar = Snackbar.make(
                layoutMain!!,
                getString(R.string.generic_error_message),
                Snackbar.LENGTH_LONG
            )
            snackbar.show()
        }
    }
}