package com.jobsity.challenge.ui.model.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.jobsity.challenge.R
import com.jobsity.challenge.connection.protocol.TvMazeTags
import com.jobsity.challenge.utils.ChallengeUtils
import org.json.JSONArray
import org.json.JSONObject

class CrewCreditsAdapter (private val context: Context, val jsonTvShows: JSONArray, val onItemClickListener: OnItemClickListener) : RecyclerView.Adapter<CrewCreditsAdapter.HolderTvShow>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HolderTvShow {
        val inflater = LayoutInflater.from(parent.context)
        return HolderTvShow(context, inflater, parent)
    }

    override fun onBindViewHolder(holder: HolderTvShow, position: Int) {
        val jsonTvShow = jsonTvShows.getJSONObject(position)
        holder.bind(position, jsonTvShow, onItemClickListener)
    }

    override fun getItemCount(): Int = jsonTvShows.length()

    interface OnItemClickListener {
        fun onItemClick(jsonTvShow: JSONObject)
    }

    /**
     *
     */
    class HolderTvShow(private val contexto: Context, inflater: LayoutInflater, parent: ViewGroup) :
        RecyclerView.ViewHolder(inflater.inflate(R.layout.item_search_tv_show, parent, false)) {

        private var imgvPoster : ImageView? = null
        private var tvName: TextView? = null

        init {
            imgvPoster = itemView.findViewById(R.id.imgv_poster) as ImageView
            tvName = itemView.findViewById(R.id.tv_name)
        }

        fun bind(position: Int, json: JSONObject, onItemClickListener: OnItemClickListener) {

            val jsonTvShow = json.getJSONObject(TvMazeTags.TAG_EMBEDDED).getJSONObject(TvMazeTags.TAG_SHOW)

            itemView.setOnClickListener { onItemClickListener.onItemClick(jsonTvShow) }

            val urlImagen = ChallengeUtils.getDefaultTvShowPoster(jsonTvShow)
            ChallengeUtils.downloadImage(urlImagen, imgvPoster!!, R.drawable.ic_image)

            tvName?.text = jsonTvShow.getString(TvMazeTags.TAG_NAME)
        }
    }
}