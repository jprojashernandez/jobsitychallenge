package com.jobsity.challenge.ui.model.objects

import com.jobsity.challenge.ui.screens.BaseFragment

class MenuItem (val image: Int,
                val text: String,
                val title: String,
                val fragment: BaseFragment
){

    var selected: Boolean = false

}