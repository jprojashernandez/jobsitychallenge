package com.jobsity.challenge.ui.screens.splash

import android.app.KeyguardManager
import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import androidx.biometric.BiometricManager
import androidx.biometric.BiometricPrompt
import androidx.core.content.ContextCompat
import com.jobsity.challenge.R
import com.jobsity.challenge.ui.screens.MainActivity

class SplashActivity : AppCompatActivity() {
    private val TAG = SplashActivity::class.java.simpleName

    private var supportedBiometric = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        // Biometria
        validateBiometricSupport()

        // Show Biometric promp
        showDelayedBiometricPrompt()
    }

    private fun showDelayedBiometricPrompt() {
        Handler().postDelayed({
            biometricAuth()
        }, 2000)
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Biometric
    ////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Method to validate if device has configured the authenticate using biometrics
     */
    private fun validateBiometricSupport() {
        val biometricManager = BiometricManager.from(this)
        when (biometricManager.canAuthenticate()) {
            BiometricManager.BIOMETRIC_SUCCESS -> {
                supportedBiometric = true
                Log.d(TAG, "App can authenticate using biometrics.")
            }
            BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE ->
                Log.e(TAG, "No biometric features available on this device.")
            BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE ->
                Log.e(TAG, "Biometric features are currently unavailable.")
            BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED ->
                Log.e(TAG, "The user hasn't associated any biometric credentials with their account.")
        }
    }

    /**
     * Method to show the prompt
     */
    private fun biometricAuth() {
        val executor = ContextCompat.getMainExecutor(this)
        val biometricPrompt = BiometricPrompt(this, executor,
            object : BiometricPrompt.AuthenticationCallback() {
                override fun onAuthenticationSucceeded(result: BiometricPrompt.AuthenticationResult) {
                    super.onAuthenticationSucceeded(result)
                    navigateToMainActivity()
                }

                override fun onAuthenticationError(errorCode: Int, errString: CharSequence) {
                    // Si es que no tiene ninguna seguridad en su dispositivo -> Se procede con la compra
                    // If the device does not have security in device
                    if (!supportedBiometric) {
                        val kgManager = getSystemService(Context.KEYGUARD_SERVICE) as KeyguardManager
                        if (!kgManager.isKeyguardSecure)
                            navigateToMainActivity()
                    }
                }
            })

        var prompInfoBuilder = BiometricPrompt.PromptInfo.Builder()
            .setTitle(getString(R.string.app_name))
            .setSubtitle(getString(R.string.splash_auth_message))
            .setDeviceCredentialAllowed(true)

        var promptInfo = prompInfoBuilder.build()
        biometricPrompt.authenticate(promptInfo)
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Navigation
    ////////////////////////////////////////////////////////////////////////////////////////////////

    fun navigateToMainActivity() {
        val intent = MainActivity.newInstance(this)
        startActivity(intent)
        finish()
    }
}