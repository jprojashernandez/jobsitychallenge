package com.jobsity.challenge.ui.model.adapters

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import com.jobsity.challenge.R
import com.jobsity.challenge.ui.model.objects.MenuItem

class SideMenuAdapter(val context: Context, val listItems: MutableList<MenuItem>): BaseAdapter() {

    private val inflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    override fun getCount(): Int {
        return listItems.size
    }

    override fun getItem(position: Int): MenuItem {
        return listItems.get(position)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {

        val itemMenu = listItems.get(position)
        var rowView = inflater.inflate(R.layout.item_side_menu, parent, false)

        val cvIndicador = rowView.findViewById(R.id.item_menu_lateral_cvIndicador) as CardView
        val imgvImagen = rowView.findViewById(R.id.item_menu_lateral_imgvImagen) as ImageView
        val tvTexto = rowView.findViewById(R.id.item_menu_lateral_tvTexto) as TextView

        imgvImagen.setImageDrawable(ContextCompat.getDrawable(context, itemMenu.image))
        tvTexto.text = itemMenu.text

        if (itemMenu.selected) {
            imgvImagen.setColorFilter(context.resources.getColor(R.color.helper))
            cvIndicador.setCardBackgroundColor(context.resources.getColor(R.color.helper_10))
            tvTexto.setTextColor(context.resources.getColor(R.color.helper))
        }
        else {
            cvIndicador.setCardBackgroundColor(Color.WHITE)
            imgvImagen.setColorFilter(context.resources.getColor(R.color.gray))
            tvTexto.setTextColor(context.resources.getColor(R.color.gray))
        }

        return rowView
    }

    fun selectMenuItem(position: Int)  {
        for (x in 0 until listItems.size) {
            val menuItem = listItems.get(x)
            menuItem.selected = x == position
        }

        notifyDataSetChanged()
    }
}