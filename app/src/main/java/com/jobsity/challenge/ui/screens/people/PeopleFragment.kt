package com.jobsity.challenge.ui.screens.people

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.jobsity.challenge.R
import com.jobsity.challenge.connection.TvMazeClient
import com.jobsity.challenge.connection.protocol.TvMazeTags
import com.jobsity.challenge.ui.model.adapters.PeopleAdapter
import com.jobsity.challenge.ui.screens.BaseFragment
import com.jobsity.challenge.ui.screens.show.TvShowDetailActivity
import kotlinx.coroutines.launch
import org.json.JSONArray
import org.json.JSONObject


class PeopleFragment  : BaseFragment() {

    private var jsonPeople = JSONArray()

    lateinit var rvPeople: RecyclerView
    private var peopleAdapter: PeopleAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val viewMain = inflater.inflate(R.layout.fragment_search, container, false)
        bindToView(viewMain)

        return viewMain
    }

    override fun bindToView(viewMain: View) {
        super.bindToView(viewMain)

        val svPeople = viewMain.findViewById<SearchView>(R.id.sv_shows)
        svPeople.isIconified = false
        svPeople.queryHint = getString(R.string.people_text_hint)
        svPeople.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextChange(newText: String): Boolean {
                searchPeople(newText)
                return false
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                return false
            }
        })

        rvPeople = viewMain.findViewById(R.id.rv_shows)
        rvPeople.addItemDecoration(DividerItemDecoration(rvPeople.context, LinearLayoutManager.VERTICAL))
    }

    private fun updateContent(jsonTvShows: JSONArray) {
        this.jsonPeople = jsonTvShows

        peopleAdapter = PeopleAdapter(requireContext(), this.jsonPeople, object: PeopleAdapter.OnItemClickListener {
            override fun onItemClick(jsonPerson: JSONObject) {
                navigateToPersonDetailActivity(jsonPerson)
            }
        })
        rvPeople.adapter = peopleAdapter

        showContent()
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Navigation
    ////////////////////////////////////////////////////////////////////////////////////////////////

    fun navigateToPersonDetailActivity(jsonPerson: JSONObject) {
        val intent = PersonDetailActivity.newInstance(requireContext(), jsonPerson.toString())
        startActivity(intent)
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Service
    ////////////////////////////////////////////////////////////////////////////////////////////////

    private fun searchPeople(text: String) {
        showLoading()

        lifecycleScope.launch {
            val response = TvMazeClient.searchPeople(text)

            if (response.success) {
                updateContent(response.response!!.getJSONArray(TvMazeTags.TAG_PEOPLE))
            }
            else {
                showSnackError()
            }
        }
    }

}