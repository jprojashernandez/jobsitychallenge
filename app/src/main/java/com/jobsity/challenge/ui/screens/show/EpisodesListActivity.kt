package com.jobsity.challenge.ui.screens.show

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.jobsity.challenge.R
import com.jobsity.challenge.connection.TvMazeClient
import com.jobsity.challenge.connection.protocol.TvMazeTags
import com.jobsity.challenge.ui.model.adapters.EpisodesAdapter
import com.jobsity.challenge.ui.model.adapters.SeasonsAdapter
import com.jobsity.challenge.ui.screens.BaseActivity
import kotlinx.coroutines.launch
import org.json.JSONArray
import org.json.JSONObject

class EpisodesListActivity : BaseActivity() {
    private lateinit var jsonSeason: JSONObject
    lateinit var jsonEpisodes: JSONArray

    private lateinit var rvEpisodes: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_episodes_list)

        val jsonStrSeason = intent.getStringExtra("jsonSeason")
        jsonSeason = JSONObject(jsonStrSeason)

        bindToView()
        requestEpisodes()
    }

    override fun bindToView() {
        super.bindToView()

        rvEpisodes = findViewById(R.id.rv_episodes)
        rvEpisodes.addItemDecoration(DividerItemDecoration(rvEpisodes.context, LinearLayoutManager.VERTICAL))
    }

    private fun updateContent(jsonEpisodes: JSONArray) {
        val episodesAdapter = EpisodesAdapter(this, jsonEpisodes)
        rvEpisodes.adapter = episodesAdapter

        showContent()
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Service
    ////////////////////////////////////////////////////////////////////////////////////////////////

    private fun requestEpisodes() {
        showLoading()

        lifecycleScope.launch {
            val response = TvMazeClient.getEpisodes(jsonSeason.getInt(TvMazeTags.TAG_ID))

            if (response.success) {
                jsonEpisodes = response.response!!.getJSONArray(TvMazeTags.TAG_EPISODES)
                updateContent(jsonEpisodes!!)
            }
            else {
                showError()
            }
        }
    }

    companion object {
        fun newInstance(context: Context, jsonSeason: String): Intent {
            val intent = Intent(context, EpisodesListActivity::class.java)
            intent.putExtra("jsonSeason", jsonSeason)
            return intent
        }
    }
}