package com.jobsity.challenge.ui.model.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.jobsity.challenge.R
import com.jobsity.challenge.connection.protocol.TvMazeTags
import com.jobsity.challenge.utils.ChallengeUtils
import org.json.JSONArray
import org.json.JSONObject

class TvShowsHomeAdapter (private val context: Context, val isHome: Boolean,
                          val jsonTvShows: JSONArray, val onItemClickListener: OnItemClickListener) : RecyclerView.Adapter<TvShowsHomeAdapter.HolderTvShow>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HolderTvShow {
        val inflater = LayoutInflater.from(parent.context)

        val layout = if (isHome) {
            R.layout.item_home_tv_show
        } else {
            R.layout.item_search_tv_show
        }

        return HolderTvShow(context, isHome, inflater, parent, layout)
    }

    override fun onBindViewHolder(holder: HolderTvShow, position: Int) {
        val jsonBanner = jsonTvShows.getJSONObject(position)
        holder.bind(position, jsonBanner, onItemClickListener)
    }

    override fun getItemCount(): Int = jsonTvShows.length()

    interface OnItemClickListener {
        fun onItemClick(jsonProducto: JSONObject)
    }

    /**
     *
     */
    class HolderTvShow(private val contexto: Context, val isHome: Boolean, inflater: LayoutInflater, parent: ViewGroup, idLayout: Int) :
        RecyclerView.ViewHolder(inflater.inflate(idLayout, parent, false)) {

        private var imgvPoster : ImageView? = null
        private var tvName: TextView? = null

        init {
            imgvPoster = itemView.findViewById(R.id.imgv_poster) as ImageView
            tvName = itemView.findViewById(R.id.tv_name)
        }

        fun bind(position: Int, jsonTvShow: JSONObject, onItemClickListener: OnItemClickListener) {

            val jsonShow = if (isHome) {
                jsonTvShow
            } else {
                jsonTvShow.getJSONObject("show")
            }

            itemView.setOnClickListener { onItemClickListener.onItemClick(jsonShow) }

            val urlImagen = ChallengeUtils.getDefaultTvShowPoster(jsonShow)
            ChallengeUtils.downloadImage(urlImagen, imgvPoster!!, R.drawable.ic_image)

            tvName?.text = jsonShow.getString(TvMazeTags.TAG_NAME)

            if (isHome) {
                itemView.requestLayout();
                itemView.layoutParams.height = if (position % 2 == 0) { 900 } else { 600 }
                itemView.requestLayout()
            }
        }
    }
}