package com.jobsity.challenge.ui.screens.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.NestedScrollView
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.jobsity.challenge.R
import com.jobsity.challenge.connection.TvMazeClient
import com.jobsity.challenge.connection.protocol.TvMazeTags
import com.jobsity.challenge.ui.screens.BaseFragment
import com.jobsity.challenge.ui.model.adapters.TvShowsHomeAdapter
import com.jobsity.challenge.ui.screens.show.TvShowDetailActivity
import kotlinx.coroutines.launch
import org.json.JSONArray
import org.json.JSONObject

/**
 * Fragment to show the list of tv shows. This is the app home
 */
class HomeFragment : BaseFragment() {
    private val TAG = HomeFragment::class.java.simpleName

    private var jsonTvShows: JSONArray? = null

    private lateinit var rvShows: RecyclerView

    private lateinit var tvShowsHomeAdapter: TvShowsHomeAdapter
    private var currentPage = 1
    private var gettingTvShows: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val viewMain = inflater.inflate(R.layout.fragment_home, container, false)
        bindToView(viewMain)

        // Send the request to the service to load the tv show list
        requestTvShows();

        return viewMain
    }

    override fun bindToView(viewMain: View) {
        super.bindToView(viewMain)

        rvShows = viewMain.findViewById(R.id.rv_shows)
        rvShows.layoutManager = StaggeredGridLayoutManager(2, 1)

        // Listener for paginated requests. Infinite scroll
        (layoutContainer as NestedScrollView).setOnScrollChangeListener(NestedScrollView.OnScrollChangeListener { nestedScrollView, scrollX, scrollY, oldScrollX, oldScrollY ->
            val view: View = nestedScrollView.getChildAt(nestedScrollView.childCount - 1)
            val bottomDetector: Int =
                view.bottom - (nestedScrollView.height + nestedScrollView.scrollY)
            if (bottomDetector == 0) {
                requestTvShowsPaginated()
            }
        })
    }

    private fun updateContent(jsonTvShows: JSONArray) {

        val onItemClickListenerHome = object: TvShowsHomeAdapter.OnItemClickListener {
            override fun onItemClick(jsonTvShow: JSONObject) {
                navigateToTvShowDetailActivity(jsonTvShow)
            }
        }

        tvShowsHomeAdapter = TvShowsHomeAdapter(
            requireContext(),
            true,
            jsonTvShows!!,
            onItemClickListenerHome
        )
        rvShows.adapter = tvShowsHomeAdapter

        showContent()
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Navigation
    ////////////////////////////////////////////////////////////////////////////////////////////////

    fun navigateToTvShowDetailActivity(jsonTvShow: JSONObject) {
        val intent = TvShowDetailActivity.newInstance(requireContext(), jsonTvShow.toString())
        startActivity(intent)
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Service
    ////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Method to request the first page of tv shows. This method is called when the fragment is
     * loaded in first time
     */
    private fun requestTvShows() {
        showLoading()

        lifecycleScope.launch {
            val response = TvMazeClient.getTvShows()

            if (response.success) {
                jsonTvShows = response.response!!.getJSONArray(TvMazeTags.TAG_SHOWS)
                updateContent(jsonTvShows!!)
            }
            else {
                if(jsonTvShows != null) {
                    updateContent(jsonTvShows!!)
                    showSnackError()
                }
                else {
                    showError()
                }
            }
        }
    }

    /**
     * Method to request the second+ page of tv shows. When the response is received, the new
     * array of tv shows is appended to the main array
     */
    private fun requestTvShowsPaginated() {
        if(!gettingTvShows) {
            gettingTvShows = true

            lifecycleScope.launch {
                val response = TvMazeClient.getTvShows(currentPage)

                if (response.success) {
                    val jsonPaginatedTvShows = response.response!!.getJSONArray(TvMazeTags.TAG_SHOWS)
                    if(jsonPaginatedTvShows.length() > 0) {
                        for(x in 0 until jsonPaginatedTvShows.length()) {
                            val jsonTvShow = jsonPaginatedTvShows.getJSONObject(x)
                            jsonTvShows!!.put(jsonTvShow)
                        }
                        tvShowsHomeAdapter.notifyDataSetChanged()
                        currentPage++
                    }
                }

                gettingTvShows = false
            }
        }
    }
}