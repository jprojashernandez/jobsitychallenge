package com.jobsity.challenge.ui.model.adapters

import android.content.Context
import android.os.Build
import android.text.Html
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.jobsity.challenge.R
import com.jobsity.challenge.connection.protocol.TvMazeTags
import com.jobsity.challenge.utils.ChallengeUtils
import org.json.JSONArray
import org.json.JSONObject

class EpisodesAdapter (private val context: Context,
                       val jsonEpisodes: JSONArray) : RecyclerView.Adapter<EpisodesAdapter.HolderSeason>() {

    private val inflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HolderSeason {
        val inflater = LayoutInflater.from(parent.context)
        return HolderSeason(context, inflater, parent)
    }

    override fun onBindViewHolder(holder: HolderSeason, position: Int) {
        val jsonCategoria = jsonEpisodes.getJSONObject(position)
        holder.bind(position, jsonCategoria)
    }

    override fun getItemCount(): Int = jsonEpisodes.length()

    /**
     *
     */
    class HolderSeason(private val contexto: Context, inflater: LayoutInflater, parent: ViewGroup) :
        RecyclerView.ViewHolder(inflater.inflate(R.layout.item_episode, parent, false)) {

        private var imgvImage: ImageView? = null
        private var tvName: TextView? = null
        private var tvSeason: TextView? = null
        private var tvSummary: TextView? = null

        init {
            imgvImage = itemView.findViewById(R.id.imgv_image) as ImageView
            tvName = itemView.findViewById(R.id.tv_name) as TextView
            tvSeason = itemView.findViewById(R.id.tv_season) as TextView
            tvSummary = itemView.findViewById(R.id.tv_summary) as TextView
        }

        fun bind(position: Int, jsonEpisode: JSONObject) {
            val imageUrl = ChallengeUtils.getDefaultTvShowPoster(jsonEpisode)
            ChallengeUtils.downloadImage(imageUrl, imgvImage!!, R.drawable.ic_image)

            tvName?.text = String.format("Espisode #%s: %s", jsonEpisode.getInt(TvMazeTags.TAG_NUMBER),
                jsonEpisode.getString(TvMazeTags.TAG_NAME))

            tvSeason?.text = String.format("Season %s", jsonEpisode.getInt(TvMazeTags.TAG_SEASON))

            val summary = jsonEpisode.getString(TvMazeTags.TAG_SUMMARY)
            tvSummary?.text = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                Html.fromHtml(summary, Html.FROM_HTML_MODE_COMPACT)
            } else {
                Html.fromHtml(summary)
            }
        }
    }
}