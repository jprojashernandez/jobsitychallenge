package com.jobsity.challenge.ui.screens.search

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.jobsity.challenge.R
import com.jobsity.challenge.connection.TvMazeClient
import com.jobsity.challenge.connection.protocol.TvMazeTags
import com.jobsity.challenge.ui.model.adapters.TvShowsHomeAdapter
import com.jobsity.challenge.ui.screens.BaseFragment
import com.jobsity.challenge.ui.screens.show.TvShowDetailActivity
import kotlinx.coroutines.launch
import org.json.JSONArray
import org.json.JSONObject


class SearchFragment : BaseFragment() {

    private var jsonTvShows = JSONArray()

    lateinit var rvShows: RecyclerView
    private var tvShowsSearchAdapter: TvShowsHomeAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val viewMain = inflater.inflate(R.layout.fragment_search, container, false)
        bindToView(viewMain)

        return viewMain
    }

    override fun bindToView(viewMain: View) {
        super.bindToView(viewMain)

        val svShows = viewMain.findViewById<SearchView>(R.id.sv_shows)
        svShows.isIconified = false
        svShows.queryHint = getString(R.string.search_text_hint)
        svShows.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextChange(newText: String): Boolean {
                searchShows(newText)
                return false
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                return false
            }
        })

        rvShows = viewMain.findViewById(R.id.rv_shows)
        rvShows.addItemDecoration(DividerItemDecoration(rvShows.context, LinearLayoutManager.VERTICAL))
    }

    private fun updateContent(jsonTvShows: JSONArray) {
        this.jsonTvShows = jsonTvShows

        tvShowsSearchAdapter = TvShowsHomeAdapter(requireContext(), false, this.jsonTvShows, object: TvShowsHomeAdapter.OnItemClickListener {
            override fun onItemClick(jsonTvShow: JSONObject) {
                navigateToTvShowDetailActivity(jsonTvShow)
            }
        })
        rvShows.adapter = tvShowsSearchAdapter

        showContent()
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Navigation
    ////////////////////////////////////////////////////////////////////////////////////////////////

    fun navigateToTvShowDetailActivity(jsonTvShow: JSONObject) {
        val intent = TvShowDetailActivity.newInstance(requireContext(), jsonTvShow.toString())
        startActivity(intent)
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Service
    ////////////////////////////////////////////////////////////////////////////////////////////////

    private fun searchShows(text: String) {
        showLoading()

        lifecycleScope.launch {
            val response = TvMazeClient.searchShows(text)

            if (response.success) {
                updateContent(response.response!!.getJSONArray(TvMazeTags.TAG_SHOWS))
            }
            else {
                showSnackError()
            }
        }
    }

}