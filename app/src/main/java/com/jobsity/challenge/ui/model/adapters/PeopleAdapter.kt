package com.jobsity.challenge.ui.model.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.jobsity.challenge.R
import com.jobsity.challenge.connection.protocol.TvMazeTags
import com.jobsity.challenge.utils.ChallengeUtils
import org.json.JSONArray
import org.json.JSONObject

class PeopleAdapter (private val context: Context, val jsonPeople: JSONArray,
                     val onItemClickListener: OnItemClickListener) : RecyclerView.Adapter<PeopleAdapter.HolderPerson>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HolderPerson {
        val inflater = LayoutInflater.from(parent.context)
        return HolderPerson(context, inflater, parent)
    }

    override fun onBindViewHolder(holder: HolderPerson, position: Int) {
        val jsonPerson = jsonPeople.getJSONObject(position)
        holder.bind(position, jsonPerson, onItemClickListener)
    }

    override fun getItemCount(): Int = jsonPeople.length()

    interface OnItemClickListener {
        fun onItemClick(jsonPerson: JSONObject)
    }

    /**
     *
     */
    class HolderPerson(private val contexto: Context, inflater: LayoutInflater, parent: ViewGroup) :
        RecyclerView.ViewHolder(inflater.inflate(R.layout.item_search_tv_show, parent, false)) {

        private var imgvPoster : ImageView? = null
        private var tvName: TextView? = null

        init {
            imgvPoster = itemView.findViewById(R.id.imgv_poster) as ImageView
            tvName = itemView.findViewById(R.id.tv_name)
        }

        fun bind(position: Int, json: JSONObject, onItemClickListener: OnItemClickListener) {
            val jsonPerson = json.getJSONObject(TvMazeTags.TAG_PERSON)

            itemView.setOnClickListener { onItemClickListener.onItemClick(jsonPerson) }

            val urlImagen = ChallengeUtils.getDefaultTvShowPoster(jsonPerson)
            ChallengeUtils.downloadImage(urlImagen, imgvPoster!!, R.drawable.ic_image)

            tvName?.text = jsonPerson.getString(TvMazeTags.TAG_NAME)
        }
    }
}