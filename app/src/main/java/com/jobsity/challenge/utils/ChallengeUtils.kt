package com.jobsity.challenge.utils

import android.text.TextUtils
import android.util.Log
import android.widget.ImageView
import com.jobsity.challenge.connection.protocol.TvMazeTags
import com.squareup.picasso.Picasso
import org.json.JSONObject
import java.lang.StringBuilder

object ChallengeUtils {

    fun downloadImage(imageUrl: String, imageView: ImageView, idPlacholder: Int?) {
        if(!TextUtils.isEmpty(imageUrl)) {
            try {
                val picasso = Picasso.get().load(imageUrl)
                with(picasso) {
                    idPlacholder?.let { placeholder(idPlacholder) }
                    into(imageView)
                }
            }
            catch (exception: Exception) {
                Log.e("UIUtils", "Error en Picasso", exception)
            }
        }
    }

    fun getDefaultTvShowPoster(jsonTvShow: JSONObject): String {
        val jsonImage = jsonTvShow.optJSONObject(TvMazeTags.TAG_IMAGE)
        if (jsonImage != null) {
            return jsonImage.optString(TvMazeTags.TAG_MEDIUM, "")
        }

        return ""
    }

    fun getGenresText(jsonTvShow: JSONObject): String {
        val jsonGenres = jsonTvShow.getJSONArray(TvMazeTags.TAG_GENRES)
        val sb = StringBuilder()

        for(x in 0 until jsonGenres.length()) {
            sb.append(jsonGenres.getString(x))
            if (x < jsonGenres.length() - 1)
                sb.append(" | ")
        }

        return sb.toString()
    }

    fun getScheduleText(jsonTvShow: JSONObject): String {
        val jsonSchedule = jsonTvShow.getJSONObject(TvMazeTags.TAG_SCHEDULE)
        val sb = StringBuilder(jsonSchedule.getString(TvMazeTags.TAG_TIME))

        val jsonDays = jsonSchedule.getJSONArray(TvMazeTags.TAG_DAYS)
        sb.append(" (")

        for(x in 0 until jsonDays.length()) {
            sb.append(jsonDays.getString(x))
            if (x < jsonDays.length() - 1)
                sb.append(", ")
        }

        sb.append(")")
        return sb.toString()
    }

    fun getCountryText(jsonPerson: JSONObject): String {
        val jsonCountry = jsonPerson.optJSONObject(TvMazeTags.TAG_COUNTRY)
        if (jsonCountry != null)
            return jsonCountry.getString(TvMazeTags.TAG_NAME)

        return ""
    }
}